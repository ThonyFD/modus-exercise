import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ng-e-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLogged: boolean;

  constructor() {
    this.isLogged = false;
  }

  onChange(event) {
    this.isLogged = event;
  }

  ngOnInit() {
  }

}
