import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';
import {Hero} from '../../../../../http/src/app/heroes/hero';

@Component({
  selector: 'ng-e-app-content',
  templateUrl: './app-content.component.html',
  providers: [UserService],
  styleUrls: ['./app-content.component.scss']
})
export class AppContentComponent implements OnInit {
  user: User = {
    firstName: 'Ahsan',
    lastName: 'Ayaz'
  };

  users: User[];

  @Input('isLoggedIn')
  isLoggedIn: boolean;

  @Output('change')
  change: EventEmitter<boolean> = new EventEmitter();

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.search();
  }

  search() {

    this.userService.searchUsers()
      .subscribe(users => this.users = users);

  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the user in
   */
  login() {
    this.change.emit(true);
  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the user out
   */
  logout() {
    this.change.emit(false);
  }

}
